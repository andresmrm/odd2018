# Apresentação

Apresentação para o Open Data Day - UFRN.

[Apresentação](https://andresmrm.gitlab.io/odd2018)

[Página comum](https://andresmrm.gitlab.io/odd2018/pagina.html)

# Compilar

Precisa de pandoc.

Para compilar uma vez:

	./run.sh
    
Para compilar quando `apre.md` mudar:

    echo apre.md | entr ./run.sh
