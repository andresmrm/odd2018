---
title: Discussão sobre iniciativas de Dados Governamentais Abertos
subtitle: Open Data Day - UFRN
date: 01/03/2018
lang: pt-BR
author: Andrés M. R. Martano
theme: solarized
---


# Dados Abertos

[![](imgs/5star.png){height=500px}](http://5stardata.info)

# Dados Governamentais Abertos

Os [8 princípios](https://opengovdata.org) (2007):

1. Completos
2. Primários
3. Atuais
4. Acessíveis
5. Processáveis por máquina
6. Sem restrições de acesso
7. Formato não proprietário
8. Licença livre

---

- Buscam **favorecer o reúso** dos dados
- Muitos potenciais benefícios
- Dependem da apropriação pela sociedade

# Open data: Empowering the empowered or effective data use for everyone?

[Artigo](http://firstmonday.org/ojs/index.php/fm/article/view/3316/2764) de Michael Gurstein fala sobre alguns dos riscos. Se antes já existia a exclusão digital, agora temos a exclusão no uso dos dados.

# O Caso de Zanesville (EUA)

Dados (sobre a rede de abastecimento de água e sobre os moradores das casas) foram usados para provar que a prefeitura estava deliberadamente se recusando a levar água encanada a uma comunidade predominantemente negra. Prefeitura multada em $11 milhões.

Caso citado por Tim Berners Lee em uma [TED Talk](https://www.ted.com/talks/tim_berners_lee_the_year_open_data_went_worldwide), como um uso exemplar de dados abertos.

---

Gurstein argumenta que a comunidade teve ajuda de uma consultoria sem fins lucrativos especializada em análise geográfica, composta por vários Ph.D.s e o advogado que assumiu o caso era formado em Harvard.

Não há nada de errado com isso, mas nem todo mundo tem acesso a uma consultoria dessas.

# O Caso de Bangalore (India)

[Artigo](http://casumm.files.wordpress.com/2008/09/bhoomi-e-governance.pdf) analisa o impacto da digitalização de registros de propriedade de terra. Afirma que os dados foram usados por empresas e classes abastadas para se apropriar de terras das pessoas pobres.

Através dos dados identificaram oportunidades de suborno, falhas nos registros ou na documentação.

Dados abertos colaborando com mais um caso clássico onde "o de cima sobe e o de baixo desce".

---

Como nos livramos dessa "situação precária"?

Gurstein argumenta que não basta os dados estarem acessíveis para serem usados igualmente por todas as pessoas. É necessário um esforço para democratizar o uso dos dados.

---

# O Caso de Solano County (EUA)

O Centro de Pesquisas de Saúde Pública da Universidade da California realiza uma pesquisa semestral sobre a saúde no estado.

Os dados coletados são públicos, mas além disso o Centro fornece treinamentos sobre como usá-los.

---

Existia um projeto de construção de um segundo ponto de parada de caminhões na mesma região, Solano County.

Moradores, usando o treinamento que receberam no Centro, criaram um mapa mostrando que a incidência de asma na comunidade já era a maior do estado, e assim evitaram a construção da segunda parada para caminhões.

---

Gurstein argumenta que, ao contrário do caso em Zanesville, em Solano County o próprio disponibilizador dos dados se preocupou com a apropriação deles pela sociedade.

---

Ele defende que para o uso dos dados é necessário:

- Acesso à Internet
- Computadores e programas adequados
- Capacitação no uso dos programas
- Dados estruturados no formato adequado
- Capacidade de interpretação dos dados
- Capacidade de defender seu ponto de vista
- Arcabouço legal embasando o uso dos dados


# Exemplos de iniciativas envolvendo Dados Governamentais Abertos


# Cuidando do Meu Bairro

Plataforma, desenvolvida desde 2011 pelo [Colab-USP](https://colab.each.usp.br), que mapeia o orçamento da cidade de São Paulo-SP, facilitando inclusive o envio de pedidos de acesso à informação.

[cuidando.vc](https://cuidando.vc)

---

- Usa dados de execução orçamentária disponibilizados pela prefeitura
- Parceria com o [Observatório Social de São Paulo](http://saopaulo.osbrasil.org.br/)
- Dezenas de oficinas (Escola de Cidadania, Escola do Parlamento, Agente de Governo Aberto, Observatório Social)
- Prêmio de Educação Fiscal 2016 (FEBRAFITE)

---

![](imgs/cuidando.png){height=500px}

---

![](imgs/cuidando2.png){height=500px}

---

![](imgs/escola_cid.jpg){height=500px}

---

![](imgs/escola_parl.jpg){height=500px}

---

![](imgs/obsp.jpg){height=500px}

# Agentes de Governo Aberto

[Iniciativa da prefeitura](https://archive.org/stream/SPABERTA/LIVRO_SP_ABERTA#page/n157/mode/2up) de São Paulo, criada em 2015, que fornece bolsas para oficinas na temática de Governo Aberto.

Proposta de popularizar ideias do Governo Aberto.

---

- 48 oficinas por ciclo (6 meses)
- Eixos: 
  - Mapeamento Colaborativo
  - Tecnologia Aberta e Colaborativa
  - Transparência e Dados Abertos
  - Comunicação em Rede
- As próprias pessoas propõem oficinas
- Oficinas espalhadas por toda a cidade
- Desafio de casar oferta e demanda
- Oficinas para Conselhos

# Monitorando a Cidade

Plataforma e metodologia para criação de [campanhas cívicas](http://promisetracker.org) de coleta de dados, desenvolvido pelo [Center for Civic Media (MIT)](https://civic.mit.edu).

[monitorandoacidade.org](http://monitorandoacidade.org)

---

- Surge da vontade de monitorar o Programa de Metas municipal: [De Olho nas Metas](https://deolhonasmetas.org.br)
- Percebem a importância de deixar as pessoas escolherem o que monitorar

---

- Site para criação de campanhas
- Coleta de dados via celular
- Relatório automático
- Metodologia participativa
- Parceria com Colab-USP

---

- 17 oficinas ao redor do Brasil
- Campanhas de monitoramento da merenda em escolas públicas no Pará [[1](https://colab-usp.github.io/monitorando-relatorio-fase2/),[2](http://bit.ly/monitorando-sumario)]
  - Santarém: [Movimento Estudantil](https://www.facebook.com/mpepepara/) + MP
  - Belém: UFPA + CGU
  - Ponta de Pedras: MP
- Brasília: CGU
- Belo Horizonte: Prefeitura

---

[![](imgs/ato.jpg){height=500px}](http://porvir.org/especiais/participacao/alunos-fiscalizam-merenda-e-conseguem-garantir-direitos/)

---

![](imgs/reuniao.jpg){height=500px}

---

![](imgs/foto.jpg){height=500px}

---

![](imgs/app.jpg){height=500px}

---

[![](imgs/relatorio.png){height=500px}](https://monitor.promisetracker.org/pt-BR/campaigns/420/share)

---

![](imgs/degusta.jpg){height=500px}

---

![](imgs/reuniao2.jpg){height=500px}

# Radar Parlamentar

Plataforma desenvolvida, principalmente, por equipe do [PoliGNU-USP](https://polignu.org/) desde 2012, que gera visualizações e análises sobre as casas legislativas.

[radarparlamentar.polignu.org](http://radarparlamentar.polignu.org)

---

- Gráficos sobre:
  - Senado
  - Câmara dos Deputados
  - Câmara Municipal de São Paulo
- [Análise quantitativa](https://www.revistas.usp.br/leviathan/article/view/143408) sobre semelhanças entre partidos políticos com base nas votações das casas legislativas
- Parceria com disciplina da UNB (disciplina sobre manutenção de software)
- Participação em Hackathona da Câmara dos Deputados (2013) [[1](http://blog.hsvab.eng.br/2013/11/01/a-experiencia-deste-hackathon-na-camara-dos-deputados/),[2](http://www2.camara.leg.br/a-camara/programas-institucionais/programas-anteriores/hackathon/2014/deputados)]

---

![](imgs/radar.png){height=500px}

---

![](imgs/mulheres.png){height=500px}

# Diário Livre

Plataforma fruto de uma parceria entre Colab-USP e Prefeitura de São Paulo (2014), para publicar o Diário Oficial da Cidade de São Paulo em formato aberto.

[devcolab.each.usp.br/do](https://devcolab.each.usp.br/do)

---

- Proposta inicial envolvia publicação conjunta de várias outras bases
- Acumula cerca de 300mil acessos
- Publicação em HTML, XML e JSON, além de download integral da base
- [Prêmio do Congresso de Informática e Inovação na Gestão Pública 2015 (CONIP)](http://each.uspnet.usp.br/site/conteudo-imprensa-noticia.php?noticia=1988)
- Dificuldade de internalização
- Direito ao esquecimento?

# Fim

Obrigado!

Link para essa apresentação:

[andresmrm.gitlab.io/odd2018](https://andresmrm.gitlab.io/odd2018)

Grupo sobre Direito Digital:

[digitalrights.cc](http://digitalrights.cc)
